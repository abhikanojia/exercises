/* globals document */
function NumericalFormHandler(formData) {
  this.submitBtn = formData.submitBtn;
  this.form = this.submitBtn.parentElement;
  this.numberField = formData.numberField;
  this.statusField = formData.statusField;
}

NumericalFormHandler.prototype.isEmpty = function(first_argument) {
  return (this.numberField.value === "");
};

NumericalFormHandler.prototype.isValidNumber = function() {
  if(!this.isEmpty() && Number(this.numberField.value)) {
    this.statusField.value = "true";
    this.form.submit();
  } else {
    this.statusField.value = "false";
  }
};

NumericalFormHandler.prototype.init = function() {
  var _this = this;
  this.submitBtn.addEventListener('click', function(e) {
    e.preventDefault();
    _this.isValidNumber();
  });
};

var formData = {
  submitBtn: document.querySelector('[data-btn=submit]'),
  numberField: document.querySelector('[data-field=number]'),
  statusField: document.querySelector('[data-field=result]')
};

var formHandler = new NumericalFormHandler(formData);

formHandler.init();