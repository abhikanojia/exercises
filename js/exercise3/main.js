/* globals document */
function CategoryHandler(data){
  this.element = data.element;
  this.hideClass = data.hideClass;
  this.hideListClass = data.hideListClass;
  this.showListClass = data.showListClass;
  this.noneClass = data.noneClass;
  this.elementType = data.elementType;
}

CategoryHandler.prototype.showHideElement = function(item, flag) {
  if(flag) {
    item.classList.remove(this.hideListClass);
    item.classList.remove(this.hideClass);
    item.classList += this.showListClass;
  } else {
    item.classList.remove(this.showListClass);
    item.classList += this.hideClass;
  }
};

CategoryHandler.prototype.toggleCheckBox = function(element, value) {
  element.checked = value;
};

CategoryHandler.prototype.getElementByType = function(element, type) {
  var query = '[type=' + type + ']';
  return element.querySelector(query);
};

CategoryHandler.prototype.showHideListItems = function(element, showHide) {
  var childList = element.parentElement.querySelector(this.noneClass);
  if(childList) {
    var childListItems = childList.children;
    for (var i = 0; i < childListItems.length; i++) {
      this.showHideElement(childListItems[i], showHide);
      this.toggleCheckBox(this.getElementByType(childListItems[i], this.elementType), showHide);
    }
  }
};

CategoryHandler.prototype.handleCheckEvent = function() {
  var _this = this;
  return function(event) {
    if(event.target.checked === true) {
      event.target.scrollIntoView(true);
      _this.showHideListItems(event.target, true);
    } else {
      _this.showHideListItems(event.target, false);
    }
  };
};

CategoryHandler.prototype.init = function() {
  this.element.addEventListener('change', this.handleCheckEvent());
};

var data = {
  element: document.getElementById('category'),
  hideClass: "hide",
  hideListClass: "hide-li",
  showListClass: "show-li",
  noneClass: ".none",
  elementType: 'checkbox'
};

var handle = new CategoryHandler(data);

handle.init();
