function Parent(name) {
  // body...
  this.name = name;
  this.type = 'parent';
}

Parent.prototype.getName = function(first_argument) {
  // body...
  return this.name;
};

function Child(name) {
 Parent.call(this, name);
}

Child.prototype = new Parent();

var child1 = new Child("testchild");

console.log(child1.getName());
console.log(child1.type);