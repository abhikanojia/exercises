/* globals window */
/* globals alert */
/* globals prompt */
function UrlHandler() {
  this.windowSpecs = "width=400,height=450,menubar=no,location=no,resizable=no,scrollbars=no,status=no";
}

UrlHandler.prototype.init = function() {
  this.url = prompt('Please enter a url.');
  if(!this.url.trim()) {
    alert('Invalid Url.');
  } else {
    window.open(this.url,"_blank",this.windowSpecs);
  }
};

var url = new UrlHandler();
url.init();