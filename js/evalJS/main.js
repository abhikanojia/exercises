function SearchAutoComplete(data) {
  this.autoCompleteForm = data.autoCompleteForm;
  this.JsonFileUrl = data.JsonFileUrl;
}

SearchAutoComplete.prototype.matchData = function(currentObject, value) {
  return currentObject.name.toLowerCase().startsWith(value);
};

SearchAutoComplete.prototype.cacheDataFromJson = function() {
  var _this = this;
  var xmlhttpObject = new XMLHttpRequest();
  xmlhttpObject.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        _this.cache = JSON.parse(this.responseText);
        console.log(_this.cache);
      }
  };
  xmlhttpObject.open("GET", this.JsonFileUrl, true);
  xmlhttpObject.send();
};

SearchAutoComplete.prototype.isAlreadyAppended = function(parentId, value) {
  return (this.appendedNames[parentId].indexOf(value) != -1);
};

SearchAutoComplete.prototype.pushToAppendedNames = function(parentId, textValue) {
  this.appendedNames[parentId].push(textValue);
};

SearchAutoComplete.prototype.isEmpty = function(value) {
  return value == '';
};

SearchAutoComplete.prototype.searchNames = function(parentId, value) {
  var _this = this;
  if(!this.isEmpty(value)) {
    this.cache.forEach(function(object){
      if(_this.matchData(object, value) && !_this.isAlreadyAppended(parentId, object.name)) {
        _this.pushToAppendedNames(parentId, object.name);
        var listItem = document.createElement('li'),
          textElement = document.createTextNode(object.name);
        listItem.appendChild(textElement)
        _this.ul.appendChild(listItem);
      }
    })
  } else {
    this.ul.innerHTML = "";
    this.appendedNames.length = 0;
  }
};

SearchAutoComplete.prototype.showNamesSuggestion = function() {
  var _this = this;
  return function(event) {
    var element = event.target;
    _this.ul = element.nextElementSibling; // appended new ul
    _this.searchNames(element.parentElement.id, element.value);
  }
};

SearchAutoComplete.prototype.init = function() {
  var id = 0;
  var _this = this;
  this.appendedNames = {};
  this.cacheDataFromJson();
  this.autoCompleteForm.forEach(function(formElement){
    _this.appendedNames[id] = [];
    formElement.setAttribute('id', id++);
    formElement.addEventListener('keyup', _this.showNamesSuggestion());
  });
};

var data = {
  autoCompleteForm: document.querySelectorAll('[data-form=autocomplete]'),
  JsonFileUrl: 'users.json'
};

var autoComplete = new SearchAutoComplete(data);
autoComplete.init();
