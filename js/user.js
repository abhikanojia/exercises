function User(name, age) {
  this.age = age;
  this.name = name;
}

User.prototype.compare = function(user) {
 if(this.age > user.age) {
    return (this.name + " is older than " + user.name);
  } else if(this.age < user.age) {
    return (user.name + " is older than " + this.name);
  }else{
    return (this.name + " and " + user.name + " are of same age.");
  }
};

var user1 = new User("User1", 22);
var user2 = new User("User2", 24);

alert(user1.compare(user2));