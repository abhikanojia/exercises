function DaysFormHandler(allowed, checkbox, none) {
  this.allowed = allowed;
  this.checked = [];
  this.arr_checkbox = checkbox;
  this.none = none;
}

DaysFormHandler.prototype.elementAlreadyChecked = function(element) {
  if(this.checked.indexOf(element.id) ==  -1) {
    return false;
  } else {
    return true;
  }
};

DaysFormHandler.prototype.removeElement = function(element) {
  this.checked.splice(this.checked.indexOf(element.id),1);
};

DaysFormHandler.prototype.alertCheckedElements = function(){
  alert('Only 3 days can be selected. you have already selected '+ this.checked);
};

DaysFormHandler.prototype.handleElement = function(element){
  if(this.checked.length >= this.allowed) {
    this.unCheck(element);
    this.alertCheckedElements();
  } else {
    this.checked.push(element.id);
  }
};

DaysFormHandler.prototype.unCheck = function(element) {
  element.checked = false;
};

DaysFormHandler.prototype.unCheckAll = function() {
  this.checked.length = 0;
  for (var i = 0; i < this.arr_checkbox.length; i++) {
    this.arr_checkbox[i].checked = false;
  }
};

DaysFormHandler.prototype.markCheckBox = function(element) {
  this.none.checked = false;
  if(this.elementAlreadyChecked(element) && this.checked.length == 3) {
    this.removeElement(element);
  } else {
    this.handleElement(element);
  }
};


DaysFormHandler.prototype.addEvents = function() {
  var _this = this;
  for (var i = 0; i < this.arr_checkbox.length; i++) {
    this.arr_checkbox[i].addEventListener('change', function(){ _this.markCheckBox(this); }, false);
  }
  this.none.addEventListener('change', function(){ _this.unCheckAll(this); }, false);
};

var checkboxes = document.getElementsByName('day');
var none = document.getElementById('none');

var form = new DaysFormHandler(3, checkboxes, none);
form.addEvents();