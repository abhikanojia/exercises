/* globals document */
/* globals alert */
function RegistrationFormHandler(form_element, allowed_length) {
  this.form = form_element;
  this.element = this.form.elements;
  this.allowed_length = allowed_length;
  this.invalid_fields = [];
}

RegistrationFormHandler.prototype.formValid = function() {
  return !Boolean(this.invalid_fields.length);
};

RegistrationFormHandler.prototype.isChecked = function(element) {
  if(!element.checked) {
    alert(element.name + " is not checked");
    this.markInvalid(element);
  } else {
    if(this.isInvalid(element)) {
      this.markValid(element);
    }
  }
};

RegistrationFormHandler.prototype.markValid = function(element) {
  this.invalid_fields.splice(this.invalid_fields.indexOf(element.id),1);
};

RegistrationFormHandler.prototype.markInvalid = function(element) {
  this.invalid_fields.push(element.id);
};

RegistrationFormHandler.prototype.isInvalid = function(element) {
  if(this.invalid_fields.indexOf(element.id) ==  -1) {
    return false;
  } else {
    return true;
  }
};

RegistrationFormHandler.prototype.validateTextArea = function(element) {
  if(element.value.length <= this.allowed_length){
    alert(element.name + " must be min 50 characters.");
    this.markInvalid(element);
  } else {
    if(this.isInvalid(element)) {
      this.markValid(element);
    }
  }
};

RegistrationFormHandler.prototype.validateFields = function() {
  for(var i = 0;i < this.element.length; i++) {
    if(!this.element[i].value) {
      this.markInvalid(this.element[i]);
      alert(this.element[i].name + " is empty");
    }

    if(this.element[i].type === 'textarea') {
      this.validateTextArea(this.element[i]);
    }

    if(this.element[i].type === 'checkbox') {
      this.valid = this.isChecked(this.element[i]);
    }
  }
};

RegistrationFormHandler.prototype.addEvent = function(first_argument) {
  var _this = this;
  this.form.addEventListener('submit', function(e){
    e.preventDefault();
    _this.validateFields();
    if(_this.formValid()) {
      _this.form.submit();
    }
  }, false);
};

var form = document.getElementById('registration_form');
var submit_button = document.getElementById('submit-btn');
var form_handler = new RegistrationFormHandler(form, 50);

form_handler.addEvent();
