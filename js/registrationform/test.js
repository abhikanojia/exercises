function RegistrationFormHandler(data) {
  this.form = data.form;
  this.elements = data.fields;
  this.minLengthAbout = data.minLengthAbout;
  this.invalidFields = [];
  this.alerts = data.alerts;
}

RegistrationFormHandler.prototype.EmailRegex = /^[a-z0-9._]+\@\w+\.[a-z0-9]{2,3}$/;
RegistrationFormHandler.prototype.DomainRegex = /(http(s)?:\/\/(www\.)?)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;

RegistrationFormHandler.prototype.isValidUrl = function(element) {
  if(this.dataType(element) == 'url' && !this.DomainRegex.test(element.value)) {
    alert(this.alerts.inValidUrl);
    this.markInvalid(element);
  } else {
    this.markValid(element);
  }
};

RegistrationFormHandler.prototype.isChecked = function(element) {
  if(this.dataType(element) == 'checkbox' && !element.checked) {
    alert(this.alerts.checkBoxNotChecked);
    this.markInvalid(element);
  } else {
    this.markValid(element);
  }
};

RegistrationFormHandler.prototype.markValid = function(element) {
  if(!this.isInvalid(element)) {
    this.invalidFields.splice(this.invalidFields.indexOf(element.id),1);
  }
};

RegistrationFormHandler.prototype.markInvalid = function(element) {
  if(!this.isInvalid(element)) {
    this.invalidFields.push(element.id);
  }
};

RegistrationFormHandler.prototype.isInvalid = function(element) {
  return (this.invalidFields.indexOf(element.id) >=  0);
};


RegistrationFormHandler.prototype.validateTextArea = function(element) {
  if(this.dataType(element) == 'textarea' && element.value.length <= this.minLengthAbout) {
    alert(this.alerts.minLengthAbout);
    this.markInvalid(element);
  } else {
    this.markValid(element);
  }
};

RegistrationFormHandler.prototype.isValidEmail = function(element) {
  if(this.dataType(element) == 'email' && !this.EmailRegex.test(element.value)) {
    alert(this.alerts.inValidEmail);
    this.markInvalid(element);
  } else {
    this.markValid(element);
  }
};

RegistrationFormHandler.prototype.alertEmpty = function(element) {
  alert(element.id + this.alerts.empty);
};

RegistrationFormHandler.prototype.dataType = function(element) {
  return element.getAttribute('data-type');
};

RegistrationFormHandler.prototype.isEmpty = function(element) {
  return !(element.value);
};

RegistrationFormHandler.prototype.hasAttributeAndType = function(element, type) {
  return (element.hasAttribute('type') && this.dataType(element, type));
};

RegistrationFormHandler.prototype.customValidation = function(element) {
  this.isValidEmail(element);
  this.isValidUrl(element);
  this.isChecked(element);
  this.validateTextArea(element);
};

RegistrationFormHandler.prototype.validateFields = function() {
  this.invalidFields.length = 0;
  for (var formElement = 0; formElement < this.elements.length; formElement++) {
    if(this.isEmpty(this.elements[formElement])) {
      this.markInvalid(this.elements[formElement]);
      this.alertEmpty(this.elements[formElement]);
    } else {
      this.customValidation(this.elements[formElement]);
    }
  }
  return this.formIsValid();
};

RegistrationFormHandler.prototype.formIsValid = function() {
  return this.invalidFields.length === 0;
};

RegistrationFormHandler.prototype.submitFormIfValid = function(valid) {
  if(valid) {
    this.form.submit();
  }
};

RegistrationFormHandler.prototype.init = function() {
  var _this = this;
  this.form.addEventListener('submit', function(event){
    event.preventDefault();
    var valid = _this.validateFields();
    _this.submitFormIfValid(valid);
  }, false);
};

var ALERT_MESSAGES = {
  empty: " is empty",
  inValidUrl: " Invalid Homepage",
  inValidEmail: "Invalid Email",
  minLengthAbout: " About must be minimum of 50 characters",
  checkBoxNotChecked: "Receive notification is not checked."
};

var data ={
  form: document.querySelector('[data-form=form]'),
  fields: document.querySelectorAll('[data-type]'),
  minLengthAbout: 50,
  alerts: ALERT_MESSAGES
};

window.addEventListener('load', function(){
  var formHandler = new RegistrationFormHandler(data);
  formHandler.init();
});