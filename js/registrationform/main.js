/* globals alert */
function RegistrationFormHandler(data) {
  this.form = data.form;
  this.elements = this.form.elements;
  this.minLengthAbout = data.minLengthAbout;
  this.invalidFields = [];
  this.alerts = data.alerts;
}

RegistrationFormHandler.emailRegex = /^\w+@[a-zA-Z_.]+?\.[a-zA-Z]{2,3}$/;
RegistrationFormHandler.domainRegex = /((\w*)(?:\.))?((\w*(?:\.))+([^\/]*))/;


RegistrationFormHandler.prototype.isValidUrl = function(element) {
  if(!this.constructor.domainRegex.test(element.value))
  {
    alert(this.alerts.inValidUrl);
    this.markInvalid(element);
  } else {
    this.markValid(element);
  }
};

RegistrationFormHandler.prototype.isChecked = function(element) {
  if(!element.checked) {
    alert(this.alerts.checkBoxNotChecked);
    this.markInvalid(element);
  } else {
    if(this.isInvalid(element)) {
      this.markValid(element);
    }
  }
};

RegistrationFormHandler.prototype.markValid = function(element) {
  if(!this.isInvalid(element)) {
    this.invalidFields.splice(this.invalidFields.indexOf(element.id),1);
  }
};

RegistrationFormHandler.prototype.markInvalid = function(element) {
  if(!this.isInvalid(element)) {
    this.invalidFields.push(element.id);
  }
};

RegistrationFormHandler.prototype.isInvalid = function(element) {
  return (this.invalidFields.indexOf(element.id) >=  0);
};

RegistrationFormHandler.prototype.validateTextArea = function(element) {
  if(element.value.length <= this.allowedLength) {
    alert(this.alerts.minLengthAbout);
    this.markInvalid(element);
  } else {
    if(this.isInvalid(element)) {
      this.markValid(element);
    }
  }
};

RegistrationFormHandler.prototype.isValidEmail = function(element) {
  if(!this.constructor.emailRegex.test(element.value)) {
    alert(this.alerts.inValidEmail);
  }else {
    this.markValid(element);
  }
};

RegistrationFormHandler.prototype.alertEmpty = function(element) {
  alert(element.id + this.alerts.empty);
};

RegistrationFormHandler.prototype.dataType = function(element, type) {
  return element.getAttribute('data-type') === type;
};

RegistrationFormHandler.prototype.isEmpty = function(element) {
  return !(element.value);
};

RegistrationFormHandler.prototype.hasAttributeAndType = function(element, type) {
  return (element.hasAttribute('type') && this.dataType(element, type));
};

RegistrationFormHandler.prototype.validateFields = function() {
  for (var i = 0; i < this.elements.length; i++) {
    if(this.isEmpty(this.elements[i])) {

      this.markInvalid(this.elements[i]);
      this.alertEmpty(this.elements[i]);

    } else if(this.hasAttributeAndType(this.elements[i], 'email')) {

      this.isValidEmail(this.elements[i]);

    } else if(this.hasAttributeAndType(this.elements[i], 'url')) {

      this.isValidUrl(this.elements[i]);
    } else if(this.elements[i].type === 'checkbox') {
      this.isChecked(this.elements[i]);
    } else if (this.elements[i].type === 'textarea') {
      this.validateTextArea(this.elements[i]);
    }
  }
};

RegistrationFormHandler.prototype.formIsValid = function() {
  return this.invalidFields.length !== 0;
};

RegistrationFormHandler.prototype.init = function(first_argument) {
  var _this = this;
  this.form.addEventListener('submit', function(e){
    e.preventDefault();
    _this.validateFields();
  }, false);
};


var form = document.querySelector('[data-form]');
var formHandler = new RegistrationFormHandler(form, 50);

formHandler.addEvent();