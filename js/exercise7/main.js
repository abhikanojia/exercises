function ListHandler(data) {
  this.from = data.from_list;
  this.to = data.to_list;
  this.add_btn = data.add;
  this.remove_btn = data.remove;
}

ListHandler.prototype.addRemoveItems = function(from, to) {
  var removed_child = '';
  if(from.selectedIndex > -1) {
    removed_child = from.removeChild(from[from.selectedIndex]);
    to.appendChild(removed_child);
  } else {
    this.nothingSelected();
  }
};

ListHandler.prototype.addItem = function() {
  var _this = this;
  return function() {
   _this.addRemoveItems(_this.from, _this.to);
  };
};

ListHandler.prototype.nothingSelected = function(list) {
  alert('Nothing Selected.');
};

ListHandler.prototype.removeItem = function() {
  var _this = this;
  var removed_child;
  return function(){
   _this.addRemoveItems(_this.to, _this.from);
  };
};

ListHandler.prototype.init = function(data) {
  var _this = this;
  this.add_btn.addEventListener('click', _this.addItem());
  this.remove_btn.addEventListener('click', _this.removeItem());
};

var data_one = {
  from_list: document.getElementById('from_select'),
  to_list: document.getElementById('to_select'),
  add: document.getElementById('add'),
  remove: document.getElementById('remove')
};

var data_two = {
  from_list: document.getElementById('from_select_two'),
  to_list: document.getElementById('to_select_two'),
  add: document.getElementById('add_btn_two'),
  remove: document.getElementById('remove_btn_two')
};

var list = new ListHandler(data_one);
var list2 = new ListHandler(data_two);
list.init();
list2.init();