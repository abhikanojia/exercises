function Question(data) {
  this.questions = [];
  this.operators  = data.operators;
  this.calculator = data.calculator;
  this.noOfQuestions = data.noOfQuestions;
  this.numbersForQuestion = data.numbersForQuestion;
  this.questionField = data.quizContainer.find('[data-field=question]');;
}

Question.prototype.Number = 1;

Question.prototype.getRandomNumber = function() {
  return Math.ceil(Math.random() * this.numbersForQuestion);
};

Question.prototype.getQuestionObject = function(questionString, result) {
  return {
    Number: this.Number,
    questionString: questionString,
    correctAnswer: result,
    usersAnswer: null
  };
};

Question.prototype.storeQuestion = function(questionString, result) {
  this.questions.push(this.getQuestionObject(questionString, result));
};

Question.prototype.isLastQuestion = function() {
  return this.Number > this.noOfQuestions;
};

Question.prototype.storeAnswer = function(value) {
  this.questions[this.Number-1]['usersAnswer'] = value;
  this.Number++;
};

Question.prototype.validateAnswer = function(value) {
  return this.questions[this.Number-1]['correctAnswer'] == parseInt(value);
};

Question.prototype.generateQuestion = function() {
  var numberOne = this.getRandomNumber(),
    numberTwo = this.getRandomNumber(),
    operator = this.operators[Math.floor(Math.random() * this.operators.length)],
    result = this.calculator.getResult(operator, numberOne, numberTwo),
    question = numberOne.toString().concat(" ", operator, " ", numberTwo.toString());
  this.storeQuestion(question, result);
  return question;
};

Question.prototype.displayQuestion = function() {
  this.questionField.text(this.generateQuestion());
};
