function Main(data) {
  this.questionDriver = data.question;
  this.timer = data.timer;
  this.validator = data.validator;
  this.scoreBoard = data.scoreBoard;
  this.quizContainer = data.quizContainer;
  this.quizContainerForm = this.quizContainer.find(data.selectorString.quiz);
  this.startBtn = this.quizContainer.find(data.selectorString.startBtn);
  this.answerField = this.quizContainer.find(data.selectorString.answerField);
  this.submitAnswerBtn = this.quizContainer.find(data.selectorString.submitBtn);
  this.resultContainer = this.quizContainer.find(data.selectorString.result);
  this.resultElement = this.resultContainer.find(data.selectorString.correctSpan);
}

Main.prototype.startQuiz = function() {
  if(this.questionDriver.isLastQuestion()) {
    this.submitAnswerBtn.prop('disabled', true);
    this.displayResult();
    this.timer.reset();
  } else {
    this.timer.restart();
    this.submitAnswerBtn.prop('disabled', false);
    this.questionDriver.displayQuestion();
  }
};

Main.prototype.displayResult = function() {
  var _this = this;
  this.quizContainerForm.fadeOut(500, function(){
    _this.resultContainer.show();
    _this.scoreBoard.printResponseTable(_this.questionDriver.questions);
    _this.scoreBoard.displayResult(_this.resultElement);
  });
};

Main.prototype.bindEventToAnswerSubmit = function() {
  var _this = this;
  this.quizContainerForm.submit(function(event){
    event.preventDefault();
    var answer = _this.answerField.val().trim();
    _this.scoreBoard.updateScore(_this.questionDriver.validateAnswer(answer));
    _this.questionDriver.storeAnswer(answer);
    _this.answerField.val('');
    _this.startQuiz();
  });
};

Main.prototype.bindEventToStartButton = function() {
  var _this = this;
  this.startBtn.on('click', function(){
    $(this).hide();
    _this.quizContainerForm.show();
    _this.startQuiz();
  });
};

Main.prototype.init = function() {
  this.bindEventToStartButton();
  this.bindEventToAnswerSubmit();
};

$(document).ready(function(){
  var OPTIONS = {
    noOfQuestions: 2,
    numbersForQuestion: 20,
    operators: ['+', '-', '/', '*'],
    quizContainer: $('[data-div=quiz]'),
    calculator: new Calculator(),
  };

  var timerOptions = {
    end: 0,
    start: 5,
    timerElement: $('[data-field=timer]'),
    submitButton: $("[data-button=submitanswer]")
  };

  var scoreData = {
    resultContainer: $('[data-field="result"]')
  };

  var timer = new CountDownTimer(timerOptions);
  var questionDriver = new Question(OPTIONS);
  var scoreBoard = new ScoreBoard(scoreData);
  scoreBoard.init();

  var selectorDataString = {
    quiz: '[data-form=quiz]',
    startBtn: '[data-button=start]',
    answerField: '[data-field=answer]',
    submitBtn: '[data-button=submitanswer]',
    result: '[data-field=result]',
    correctSpan: 'span.correct'
  };
  var data = {
    timer: timer,
    options: OPTIONS,
    question: questionDriver,
    scoreBoard: scoreBoard,
    quizContainer: $('[data-div=quiz]'),
    selectorString: selectorDataString
  };
  var quiz = new Main(data);
  quiz.init();
});
