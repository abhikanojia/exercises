/* globals alert */
function DomainMatching(formData) {
  this.urlField = formData.urlField;
  this.submitBtn = formData.submitBtn;
  this.alerts = formData.alerts;
  this.position = formData.domainSubDomainPosition;
}

DomainMatching.prototype.MATCH_GROUP = /((\w*)(?:\.))?((\w*(?:\.))+([^\/]*))/;
DomainMatching.prototype.VALID_URL = /^((http|https):\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;

DomainMatching.prototype.isValidUrl = function() {
  return (DomainMatching.prototype.VALID_URL.test(this.urlField.value) && this.urlField.value.length < 253);
};

DomainMatching.prototype.matchGroup = function() {
  return DomainMatching.prototype.MATCH_GROUP.exec(this.urlField.value);
};

DomainMatching.prototype.printDomain = function(matches) {
  var subDomain = matches[2];
  var domain = matches[3];
  if(subDomain) {
    alert('Domain: ' + domain + ', SubDomain: '+ subDomain);
  } else {
    alert('Domain: ' + domain);
  }
};

DomainMatching.prototype.validateURL = function() {
  var _this = this;
  return function(e) {
    e.preventDefault();
    if(_this.isValidUrl()) {
      var matches = _this.matchGroup();
      _this.printDomain(matches);
    } else {
      alert(_this.alerts.INVALID_URL);
    }
  };
};

DomainMatching.prototype.init = function() {
  var _this = this;
  this.submitBtn.addEventListener('click', _this.validateURL());
};

var formData = {
  urlField: document.querySelector('[data-field=url]'),
  submitBtn: document.querySelector('[data-btn=submit]'),
  alerts: { INVALID_URL: 'Invalid Url entered' }
};

var formHandler = new DomainMatching(formData);
formHandler.init();
