/* globals alert */
/* globals document */
/* globals prompt */
function GetUserDetails() {
}

GetUserDetails.prototype.getFirstName = function() {
  while(!this.firstname || this.firstname.trim() === "") {
    this.firstname = prompt('Enter your firstname.');
  }
};

GetUserDetails.prototype.getLastName = function() {
  while(!this.lastname || this.lastname.trim() === "") {
    this.lastname = prompt('Enter your lastname.');
  }
};

GetUserDetails.prototype.printUserName = function() {
  var greeting = "Hello, " + this.firstname + " " + this.lastname;
  alert(greeting);
  document.getElementById('username').innerHTML = greeting;
};

GetUserDetails.prototype.init = function() {
  this.getFirstName();
  this.getLastName();
  this.printUserName();
};

var details = new GetUserDetails();
details.init();
