class D
  def a_method
    p "a_method"
  end
end

class A < D
  def a_method
    p "a_method"
  end
  undef_method :a_method
end

ob = A.new
ob.a_method


# ob.a_method
D.new.a_method
