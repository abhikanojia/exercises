module Mod
end

class A
  include Mod
end

class B < A
  prepend Mod
end

class D < B
  include Mod
end

class C < B
  extend Mod
  include Mod
  prepend Mod
end


p C.ancestors
p C.singleton_class.ancestors

# [Mod, C, Mod, B, A, Mod Object, Kernel, BasicObject ]

# require_relative 'modulea'

# module B
# end

# class D
#   # include A
#   prepend B
#   prepend A
# end
# p D.ancestors