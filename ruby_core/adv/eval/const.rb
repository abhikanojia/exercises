# A = 10

module Mod
  A = 23
  module C
    A = 29
    class Test
      A = 20
      module Inner
        A = 299
        p Module.nesting


      end
    end
    module D
      p Mod::A
      # p ::A
      # p B::C::Test::Inner::A
      p Module.nesting
    end
  end

  def method_name
     "me"
  end
end

class A
  class B
    module C; end
    include Mod
    p Module.nesting
    def self.meth
      p "Class method"
    end
  end
end

ob = A::B.new
p ob::method_name
A::B::meth