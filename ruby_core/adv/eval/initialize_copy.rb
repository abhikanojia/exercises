class A
  def initialize

  end

  def method
    p "method"
  end

  def initialize_copy(name)
    p "initialize copy"
  end
end


ob = A.new
d = ob.dup
# d = ob.clone