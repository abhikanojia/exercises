# module TestMod
#   def method_name
#     p "method_name"
#   end
# end
# class Include
#   def self.inc(*args)
#     p"inc called"
#     p *args
#     args.each do |module_name|
#       Module.append_features(module_name)
#     end
#   end
# end

# class A
#   Module.append_features TestMod
# end

# p A.instance_methods

module A
  # def self.append_features(name)
  #   p "gets #{name}"
  # end

  # def self.included(name)
  #   p "included #{name}"
  # end

  def method_name
    p "method_name"
  end
end

class B
  include A
end

ob = B.new

ob.method_name