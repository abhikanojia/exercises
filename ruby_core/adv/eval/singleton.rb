class A
  def meth
    p "meth from class A"
  end
end

ob = A.new

def ob.meth
  p "meth from object"
end

ob.meth