module A
  def self.const_missing(name)
    "#{name} is missing"
  end
end

class B
  def self.const_missing(name)
    p "#{name} is missing in class"
  end
end

p A::D
p B::D