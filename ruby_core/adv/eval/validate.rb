module ActiveRecordValidator
  module ClassMethods
    def validate_presence_of(*args)
      class_eval do
        @valid_objects = []
        @args = args
        attr_accessor :errors
        def initialize
          @valid = false
          @errors = Hash.new([])
        end
      end
      create_singleton_methods
    end

    def create_singleton_methods
      @args.each do |method|
        define_singleton_method("find_by_#{method}") do |arg|
          @valid_objects.select { |object| object.send(method).eql? arg }
        end
      end
    end

    def collect
      p @valid_objects
    end

    def save_object(object)
      @valid_objects.push(object)
    end

    def validate_attributes
      @args
    end

    def count
      p @valid_objects.count
    end
  end

  module InstanceMethods
    def save
      if @valid
        self.class.save_object(self)
      else
        return false
      end
      true
    end

    def valid?
      store_errors
      @valid = self.class.validate_attributes.all? { |attr| instance_variable_defined?("@#{attr}") }
    end

    def invalid?

    end

    def errors_messages
      p errors
    end

    private

    def store_errors
      self.class.validate_attributes.each do |attr|
        if !instance_variable_defined?("@#{attr}")
          errors[attr] += ['cant be blank']
        end
      end
    end
  end

  def self.included(receiver)
    receiver.extend ClassMethods
    receiver.send(:include, InstanceMethods)
  end
end

class ActiveRecord
  include ActiveRecordValidator
end

class Person < ActiveRecord
  attr_accessor :name, :age
  validate_presence_of :name, :age
end

pr = Person.new
pr.name = 'Abhishek kanojia'
# pr.age = 23
p pr.valid?
pr.save

per = Person.new
# per.name = "abhi"
per.age = 24
pr.save

pr.errors_messages
p pr

Person.collect
Person.count
p "find by name"
p Person.find_by_name("abhi")
p Person.find_by_age(23)

# p Person.methods
# p Person.methods(false)
# Person.find_by_email("as")