class A
  def initialize
    @arr = []
  end
  def add(items)
    @arr << items
  end

  def method_name

  end

  def items(&block)
    instance_eval(&block) if block_given?
  end
end

ob = A.new
ob.items do
  add("a")
  add("a")
end