class A
end

A.instance_eval do
  p self
  def met
    p "meth"
  end
end

A.class_eval do
  p self
  def method_name
    p "method_name"
  end
end

p A.instance_methods.grep /method_name/