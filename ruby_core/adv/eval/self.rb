p self

local = self
p local
# module A;end
# class << self
#   p self
#   class << self
#     include A
#     p ancestors
#   end
# end

class A
  # p self
  # class << self
  #   p ancestors
  #   def class_method_a
  #   end
  #   p self
  #   class << self
  #     p ancestors
  #     def class_method_of_singleton_class

  #     end
  #     p self
  #   end
  # end
end

# p A.methods(false)
# p A.singleton_class.instance_methods(false)
# p A.singleton_class.methods(false)
# p self.class.ancestors
p A.ancestors
p A.singleton_class.ancestors