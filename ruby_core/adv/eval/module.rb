module A; end

module B
  # include A
end

module C
end

module D
end

class F
  prepend A
  prepend B # done
  # prepend C # done
  # extend D # done
  # prepend A
end

class E < F
  # include A
end

p E.ancestors

# [ A, E, F, B, A, O, K, BO]

# [ F, B, A O, K, BO]
# [ A, C, F, B, A, Object, Kernel, BasicObject ]
# [C, A, F, B Object, Kernel, BasicObject]
# [B, F, D, A, Object, Kernel, BasicObject]
# [B, F, D, A, Object, Kernel, BasicObject]