module A
  @count = 1
  ["included", "prepended", "extended"].each do |method_name|
    define_singleton_method(method_name) do |arg|
      p "#{method_name}"
      @count += 1
    end
  end
end
module D; end

class F
  prepend A
  include D
end

class E < F
  prepend A
  prepend D
  # extend A
end

# E, C, D, A, B
p E.ancestors

# D, E, A, F, Object, kernel, bo
# p E.singleton_class.ancestors

# [E, D, C, A, B, A, Object, Kernel, BasicObject]

# module A
#   def method_name
#     "method_name"
#   end
# end

# class D
#   include A
# end

# class B < D
#   prepend A
# end

# p B.ancestors
# p B.singleton_class.ancestors

