module ObjectStore
  module ClassMethods
    SINGLETON_METHOD_FOR = [:email, :fname, :age].freeze
    def attr_accessor(*args)
      args.each do |sym|
        create_dynamic_finders(sym) if SINGLETON_METHOD_FOR.member?(sym)
        super(sym)
      end
    end

    def validate_presence_of(*arguments)
      @validate_attributes = Hash.new([])
      @validate_attributes[:presence_of] = arguments
      initialize_class
    end

    def validate_numericality_of(*arguments)
     @validate_attributes[:numericality] = arguments
    end

    def initialize_class
      class_eval do
        @valid_objects = []
        attr_accessor :errors, :valid
        def initialize
          @valid = false
          @errors = Hash.new([])
        end
      end
    end

    def create_dynamic_finders(method_name)
      define_singleton_method("find_by_#{method_name}") do |arg|
        @valid_objects.select { |object| object.send(method_name).eql?(arg) }
      end
    end

    def save_object(object)
      push_to_valid_object(object) if object.valid
      object.valid
    end

    def push_to_valid_object(object)
      @valid_objects.push(object)
    end

    def error_in_attribute(object, attribute)
      p "error_in_attribute"
      object.errors[attribute] += ['Cant be blank']
    end

    def numericality_error_in_attribute(object, attribute)
      object.errors[attribute] += ['Not numeric']
    end

    def collect
      @valid_objects
    end

    def count
      @valid_objects.length
    end

    def validate_presence(object)
      @validate_attributes[:presence_of].all? { |attribute| objet.}
    end

    def validate_numericality(object)
      @validate_attributes[:numericality].all? do |attr|
        attribute_numeric = object.send(attr.to_sym).is_a? Integer
        if !attribute_numeric
          numericality_error_in_attribute(object, attr)
        end
        attribute_numeric
      end
    end
  end

  module InstanceMethods
    def save
      if validate
        self.class.save_object(self)
      else
        "Object is not valid"
      end
    end
  end

  def self.included(receiver)
    receiver.extend(ClassMethods)
    receiver.send :include, InstanceMethods
  end
end
