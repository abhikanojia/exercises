# require objectstore
require_relative './object_store'
# class Play
class Play
  include ObjectStore
  attr_accessor :age, :fname, :email

  validate_presence_of :fname, :email
  validate_numericality_of :age

  def validate
    self.errors.clear if self.valid.eql? true
    self.class.validate_presence(self)
  end
end

ob = Play.new
ob.fname = "abc"
# ob.age = 23
ob.email = "abc@vinsol.com"
p ob

p ob.save
p ob.errors
# p Play.validate_presence_attributes
# p Play.methods.grep /find_by_/

# person2 = Play.new
# person2.fname = "ac"
# person2.age = 23
# person2.email = "abc2@vinsol.com"
# p person2

# p person2.save
# p person2.errors


# p Play.collect
# p Play.count
# p Play.find_by_fname("abc")