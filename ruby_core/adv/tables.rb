# Blueprint for differet types
class Column
  attr_reader :col_name, :options
  def initialize(col_name, *options)
    @col_name = col_name
    @options = options
  end
end

class IntegerField < Column
  def to_s
    not_null = "NOT NULL" if !@options.first[:null]
    "int("+"#{@options.first[:default].to_s}" +") #{not_null}, "
  end
end

class VarcharField < Column
  def initialize(col_name, *options)
    @col_name = col_name
    @options = [{default: 255}]
  end

  def to_s
    not_null = "NOT NULL" if !@options.first[:null]
    "varchar("+"#{@options.first[:default].to_s}" +") #{not_null},"
  end
end

class DateTimeField < Column
  def to_s
    not_null = "NOT NULL" if @options.first[:null].eql? false
    "datetime #{not_null} #{not_null}, "
  end
end

module BluePrint
  @@function_to_class_mapping = {
    integer: IntegerField,
    varchar: VarcharField,
    datetime: DateTimeField
  }

  def method_missing(method_name, *arguments)
    @columns << class_name(method_name).new(arguments.first, arguments[1])
  end

  def add_created_at
    options = {null: false}
    @columns << DateTimeField.new(:created_at, options)
  end

  private

  def class_name(name)
    @@function_to_class_mapping[name]
  end

  def field_name(value)
    @@function_to_class_mapping.key(value)
  end

  def complete_sql(sql)
    sql.chop!.chop!
    sql << " );"
  end
end

class TableCreator
  attr_reader :columns, :table_name
  include BluePrint

  @tables = []

  def initialize
    @columns = []
    add_created_at
  end

  def self.push_to_tables(table_name)
    @tables << table_name
  end

  def self.push_obj(obj)
    @tables << obj
  end

  def create_table(table_name, &block)
    @table_name = table_name
    self.class.push_obj(self)
    instance_eval(&block) if block_given?
  end

  def self.tables
    @tables
  end

  def to_sql
    sql = ''
    self.class.tables.each do |table|
      sql << " CREATE TABLE `#{table_name}` ( "
      table.columns.each do |col|
        sql << col.col_name.to_s << " "
        sql << col.to_s
      end
      complete_sql(sql)
    end
    sql
  end
end

t = TableCreator.new

p t

t.create_table :users do |x|
  x.integer :age, default: 5, null: false
  x.varchar :name
  x.integer :amount, default: 100
end

t.create_table :admins do |x|
  x.integer :age, default: 5, null: false
  x.varchar :name
  x.integer :amount, default: 100
end

p t

p "tables"
p TableCreator.tables

p t.to_sql

# #<TableCreator:0x000000018c29d0 @columns=[
#   #<DateTimeField:0x000000018c2908 @col_name="created_at", @options=[{:null=>false}]>,
#   #<IntegerField:0x000000018c24d0 @col_name=:age, @options=[{:default=>5, :null=>false}]>,
#   #<VarcharField:0x000000018c23e0 @options=[{:default=>10}]>,
#   #<IntegerField:0x000000018c2278 @col_name=:amount, @options=[{:default=>100}]>],
#   @table_name=:users>
