module BluePrint
  module ClassMethods

  end

  module InstanceMethods
    def initialize
      @tables = []
      @config = Hash.new({})
      @map = []
    end

    def add_to_fields(*col_and_attributes)
      p
      # columns =
      @config[@current_table] = { col_and_attributes.first => col_and_attributes[1] }
      # # @config.fields[col_and_attributes.first] = col_and_attributes[1]
    end

    def integer(*field)
      add_to_fields(*field)
    end

    def varchar(*field)
      add_to_fields(*field)
    end
  end

  def self.included(receiver)
    receiver.extend ClassMethods
    receiver.send :include, InstanceMethods
  end
end

class TableConfig
  attr_accessor :fields
  def initialize
    @fields = Hash.new
  end
end

class Table
  include BluePrint
  def create_table(table_name, &block)
    @tables.push(table_name)
    @current_table = table_name
    # table = table_name
    # @config[table_name.to_sym] = TableConfig.new
    instance_eval(&block) if block_given?
  end
end


tc = Table.new
tc.create_table :users do |x|
  x.integer :age , default: 5, null: false
  x.integer :amount, default: 6
  x.varchar :name, null: false
end

p tc
exit

# CREATE Table users (
#   age int(5),
#   amount int(6)
#   name varchar, not null
# )

tc.create_table :admins do |x|
  x.integer :age , default: 5
  x.integer :amount, default: 6
  x.varchar :name
end

# Table.tables
# tc.to_sql
# p TableCreator.query