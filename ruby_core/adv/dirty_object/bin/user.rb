# require dirty object
require_relative '../lib/dirty_object.rb'
# User class includes dirty object
class User
  include DirtyObject
  attr_accessor :name, :age, :email
  define_dirty_attributes :name, :age
end

u = User.new

p u.name # nil
p u.age # nil

u.changes # {}

u.name = nil
u.age = nil

u.changes # {}

# p u.instance_variable_get("@changed_attributes")

# u.name = 'TEST'
# u.age = 30

# u.changes # {:name=>[nil, "TEST"], :age=>[nil, 30]}

<<<<<<< HEAD
# u.name = 'TEST'
# u.age = 30

# u.changes # {:name=>[nil, "TEST"], :age=>[nil, 30]}

# u.save

u.changes # {}

=======
>>>>>>> 922023de21faafaf7b9f45fccb9e452a83484cf4


u.name = 'TEST'
u.age = 303



u.changes # {:name=>[nil, "TEST"], :age=>[nil, 30]}

u.save

u.changes # {}
