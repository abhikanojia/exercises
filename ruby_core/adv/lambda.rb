class A
  def lamb
    puts "1"
    l = lambda { return }
    l.call
    puts "2"
  end

  def pro
    puts "1"
    pr = proc { return }
    pr.call
    puts "2"
  end
end

p A.new.pro
p A.new.lamb