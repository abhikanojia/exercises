module Filter
  def before_filter(*args)
   @@filter = args.first
   @@options = args.select { |x| x.is_a? Hash }.first
  end

  def method_added(method_name)
    return if @filtering
    return if @@filter == method_name
    return if method_name == :initialize

    # check if given in options only
    if !@@options.nil?
      return if !@@options.empty? && !@@options[:only].include?(method_name)
    end
    @filtering = true

    add_new_method(method_name)

    @filtering = false
  end

  private

  def add_new_method(method_name)
    alias_method :"original_#{method_name}", method_name

    define_method(method_name) do |*args|
      send(@@filter)
      send(:"original_#{method_name}", *args)
    end
  end
end

class BaseController
  extend Filter
  before_filter :required_login, :only => [:test_method]

  def test_method
    p "test_method"
  end

  def required_login
    p "Login is required"
  end

  def create_something
    p "create_something"
  end

  def another_method(name)
    p name
  end
end

p BaseController.instance_methods(false)
ob = BaseController.new

ob.test_method
ob.another_method("asd")