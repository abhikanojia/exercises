module Filters
  module ClassMethods
    def before_action(*arguments)
      class_instance_methods = instance_methods(false)
      class_instance_methods.delete(arguments.first)
      class_instance_methods.each do |method_name|
        alias_method :"original_#{method_name}", method_name
        define_method(method_name) do |*args|
          self.send (arguments.first)
          self.send(:"original_#{method_name}", *args)
        end
      end
    end

    def self.method_added(name)
      p "#{name} added"
    end

    def after_action(*arguments)

    end

    def around_action(*arguments)

    end
  end

  def self.method_added(name)
    p "#{name} added"
  end

  def self.included(receiver)
    receiver.extend ClassMethods
  end
end