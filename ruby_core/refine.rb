# module StringExtension
#   refine String do
#     def alpha
#       upcase
#     end
#   end
# end

# using StringExtension

# p "hi there".alpha

module Test
  def my_method
    "method from modules"
  end
end

class MyClass
  def my_method
  "original my_method()"
  end

  def another_method
    my_method
  end
end

module MyClassRefinement
  refine MyClass do
    def my_method
      "refined my_method()"
    end
  end
end

class A
  # using MyClassRefinement
  include Test
  def my_method

  end
end

ob = A.new
class << ob
  def my_method
    "method from singleton_class"
  end
end
p ob.my_method


using MyClassRefinement

p MyClass.new.my_method
p MyClass.new.another_method
p MyClass.new.my_method
